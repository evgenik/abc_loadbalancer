package extendendtraining.casestudy.abcfinlab;

import extendendtraining.casestudy.abcfinlab.Host.Host;
import extendendtraining.casestudy.abcfinlab.Request.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadBalancer {

    private ArrayList<Host> loadBalancedHostsList;
    private BalancingAlgorithm chosenAlgorithm;

    // one of the two implemented algorithms can be chosen to determine how the LoadBalancer should alocate the incoming data from the client
    public HashMap<Request, Host> handleRequest(Request request){
        HashMap<Request, Host> requestHostEntityHashMap = chosenAlgorithm.lowestLoadBalancingAlgorithm(request, loadBalancedHostsList);
        return requestHostEntityHashMap;
    }





}
