package extendendtraining.casestudy.abcfinlab.Host;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class Host {

    // the predetermined getLoad function is provided through the usage of lombok
    // annotation @Data generates Getter and Setter

    private String serverIP;
    private Double load;



}
