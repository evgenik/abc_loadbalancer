package extendendtraining.casestudy.abcfinlab.Request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {

    private int clientIP;
    private double load;

}
