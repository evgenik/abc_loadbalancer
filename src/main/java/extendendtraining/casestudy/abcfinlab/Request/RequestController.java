package extendendtraining.casestudy.abcfinlab.Request;

import extendendtraining.casestudy.abcfinlab.*;
import extendendtraining.casestudy.abcfinlab.Host.Host;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;


@RestController
@RequestMapping("/request")
public class RequestController {

    private ArrayList<Host> loadBalancedHostsList = new ArrayList<>();
    private BalancingAlgorithm exampleAlgorithm = new BalancingAlgorithm();
    private LoadBalancer balancer = new LoadBalancer(loadBalancedHostsList, exampleAlgorithm);


    // handling incoming requests from the client and checking for not being null
    @PostMapping
    public HashMap<Request, Host> distributingRequests(@Valid @RequestBody RequestDTO requestDTO) {


        Request newRequest = new Request();
        newRequest.setLoad(requestDTO.getLoad());
        newRequest.setClientIP(requestDTO.getClientIP());

        HashMap<Request, Host> distributedRequestsToHosts = balancer.handleRequest(newRequest);


        return distributedRequestsToHosts;
    }

    // generating some random Hosts that can be used for allocating the client requests
    @PostConstruct
    public void generateHosts() {

        Host newHost1 = new Host("192.168.255.001", ((Math.random() * 0.5)+ 0.5));
        Host newHost2 = new Host("192.168.255.002" ,((Math.random() * 0.5) + 0.5));
        Host newHost3 = new Host("192.168.255.003" ,((Math.random() * 0.5)+ 0.5));
        Host newHost4 = new Host("192.168.255.004" ,((Math.random() * 0.5) + 0.5));

        loadBalancedHostsList.add(newHost1);
        loadBalancedHostsList.add(newHost2);
        loadBalancedHostsList.add(newHost3);
        loadBalancedHostsList.add(newHost4);

        System.out.println(loadBalancedHostsList);
    }

}

