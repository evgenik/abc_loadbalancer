package extendendtraining.casestudy.abcfinlab.Request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RequestDTO {

    private int clientIP;
    @NotNull(message="The request needs to have a load")
    private double load;

}
