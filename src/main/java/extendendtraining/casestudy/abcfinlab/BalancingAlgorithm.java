package extendendtraining.casestudy.abcfinlab;

import extendendtraining.casestudy.abcfinlab.Host.Host;
import extendendtraining.casestudy.abcfinlab.Request.Request;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

// contains two functions that can distribute the client request to a proper host
public class BalancingAlgorithm {

   public HashMap<Request, Host> sequentialBalancingAlgorithm(Request newRequest, ArrayList<Host> loadBalancedHostsList) {
      HashMap<Request, Host> assignedRequestsList = new HashMap<>();
      assignedRequestsList.put(newRequest, loadBalancedHostsList.get(0));
      return assignedRequestsList;
   }

   public HashMap<Request, Host> lowestLoadBalancingAlgorithm(Request newRequest, ArrayList<Host> loadBalancedHostsList) {
      HashMap<Request, Host> assignedRequestsList = new HashMap<>();
      if (newRequest.getLoad() >= 0.75) {
         // used for sorting the list downward
         loadBalancedHostsList.sort(Comparator.comparing(Host::getLoad));
         loadBalancedHostsList.get(0).getLoad();
         System.out.println(loadBalancedHostsList);

      }
      assignedRequestsList.put(newRequest, loadBalancedHostsList.get(0));
      return assignedRequestsList;
   }
}
