package extendendtraining.casestudy.abcfinlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// Created a spring boot application, that resembles a Loadbalancer.
@SpringBootApplication
public class AbcfinlabApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbcfinlabApplication.class, args);
    }


}
