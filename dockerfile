FROM openjdk:8
EXPOSE 7070
ADD /target/*.jar abc.jar
ENTRYPOINT ["java", "-jar", "/abc.jar"]
